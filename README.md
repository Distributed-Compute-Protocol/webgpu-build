# INTRODUCTION

This repository contains scripts to build and release WebGPU binaries.

## Getting The Code

To clone the
[WebGPU Build repository](https://gitlab.com/Distributed-Compute-Protocol/webgpu-build),
enter the following:

    git clone --recursive git@gitlab.com:Distributed-Compute-Protocol/webgpu-build.git

## Building

To build, enter the following:

    mkdir build
    cd build
    cmake ..
    cmake --build .

## Releasing

To create a build release, clone
[dcp-native-ci](https://gitlab.com/Distributed-Compute-Protocol/dcp-native-ci)
and read the "Releasing" section of the "README.md" file therein.
