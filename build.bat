@echo off
REM
REM @file         build.bat
REM               Build the WebGPU library. for use via N-API, into the working
REM               directory.
REM @author       Jason Erb, jason@distributive.network
REM @date         January 2022

setLocal enableExtensions

pushd %~dp0
set "SD=%CD%"
popd

echo Building WebGPU...

if not exist depot_tools (
  echo Cloning depot tools...
  call git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git || goto :exit
)
set "PATH=%CD%\depot_tools;%PATH%"
set DEPOT_TOOLS_WIN_TOOLCHAIN=0
set GYP_MSVS_VERSION=2019

echo Configuring and building Dawn...
pushd "%SD%\submodules\dawn"
echo Syncing gclient...
copy scripts\standalone.gclient .gclient
if %ERRORLEVEL% NEQ 0 goto :exit
call gclient sync -v -D --force --reset || goto :exit
echo Installing pywin32...
call python -m pip install pywin32 || goto :exit
echo Building Dawn...
call :build_dawn || goto :exit
popd

echo Setting path to Dawn...
set "PATH_TO_DAWN=%SD%\submodules\dawn"
set "PATH_TO_DAWN=%PATH_TO_DAWN:\=/%"
echo | set /p name="%PATH_TO_DAWN%" >"%SD%\submodules\webgpu\PATH_TO_DAWN"

echo Setting path to Node-API...
set "PATH_TO_NODE_API=%SD%\submodules\node"
set "PATH_TO_NODE_API=%PATH_TO_NODE_API:\=/%"
echo | set /p name="%PATH_TO_NODE_API%" >"%SD%\submodules\webgpu\PATH_TO_NODE_API"

echo Setting path to Node-Addon_API...
set "PATH_TO_NODE_ADDON_API=%SD%\submodules\node_addon_api"
set "PATH_TO_NODE_ADDON_API=%PATH_TO_NODE_ADDON_API:\=/%"
echo | set /p name="%PATH_TO_NODE_ADDON_API%" >"%SD%\submodules\webgpu\PATH_TO_NODE_ADDON_API"

echo Building WebGPU...
pushd "%SD%\submodules\webgpu"
call npm ci || goto :exit
call npm run all --dawnversion=0.0.1 || goto :exit
echo Building WebGPU Examples...
pushd examples
call npm ci || goto :exit
rmdir node_modules\webgpu\generated\0.0.1\win32\build\Release /s /q
mkdir node_modules\webgpu\generated\0.0.1\win32\build\Release
popd
copy generated\0.0.1\win32\build\Release\addon-win32.node examples\node_modules\webgpu\generated\0.0.1\win32\build\Release\addon-win32.node
if %ERRORLEVEL% NEQ 0 goto :exit
echo Examples now using this WebGPU N-API add-on instead of the npm version.
popd

echo Initializing CTS...
pushd "%SD%\submodules\webgpu\generated\0.0.1\win32\build"
pushd Release
copy addon-win32.node "%SD%\submodules\cts\third_party\dawn\index.node"
if %ERRORLEVEL% NEQ 0 goto :exit
xcopy *.dll "%SD%\submodules\cts\third_party\dawn\" /I /Y
if %ERRORLEVEL% NEQ 0 goto :exit
popd
xcopy *.lib "%SD%\submodules\cts\third_party\dawn\" /I /Y
if %ERRORLEVEL% NEQ 0 goto :exit
popd
pushd "%SD%\submodules\cts"
call npm ci || goto :exit
echo Running CTS...
call npx grunt pre || goto :exit
popd

echo Copying to install directory...
mkdir install
copy "%SD%\submodules\cts\LICENSE.txt" "install\LICENSE.cts.txt"
if %ERRORLEVEL% NEQ 0 goto :exit
copy "%SD%\submodules\dawn\LICENSE" "install\LICENSE.dawn.txt"
if %ERRORLEVEL% NEQ 0 goto :exit
copy "%SD%\submodules\webgpu\LICENSE" "install\LICENSE.webgpu.txt"
if %ERRORLEVEL% NEQ 0 goto :exit
xcopy "%SD%\submodules\webgpu\generated\0.0.1\win32\build\Release\*" install\Release /I /Y
if %ERRORLEVEL% NEQ 0 goto :exit
xcopy "%SD%\submodules\webgpu\generated\0.0.1\win32\build\*.lib" install /I /Y
if %ERRORLEVEL% NEQ 0 goto :exit

exit /b 0

:build_dawn
set args=
set args=%args% dawn_enable_vulkan_validation_layers=false
set args=%args% is_clang=false
set args=%args% is_component_build=true
set args=%args% is_debug=false
set args=%args% target_cpu=""x64""
set args=%args% use_custom_libcxx=false
echo Generating build...
call gn gen out\Shared --args="%args%" || goto :exit
echo Building...
call ninja -C out\Shared || goto :exit
goto :exit

:exit
exit /b %ERRORLEVEL%
