#!/bin/sh
#
# @file         build.sh
#               Build the WebGPU library. for use via N-API, into the working
#               directory.
# @author       Dominic Cerisano, dcerisano@distributive.network
# @author       Jason Erb, jason@distributive.network
# @date         May 2020

set -e

WD="$(pwd)"
cd "$(dirname "$0")"
SD="${PWD}"
cd "${WD}"

OS="$(uname -s | tr \"[:upper:]\" \"[:lower:]\")"
echo "Building WebGPU on OS \"${OS}\"..."

echo "Configuring depot tools..."
if [ ! -d depot_tools ]; then
  git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi
export PATH=${PWD}/depot_tools:$PATH

echo "Configuring and building Dawn..."
cd "${SD}/submodules/dawn"
echo "Syncing gclient..."
cp scripts/standalone.gclient .gclient
gclient sync -D
echo "Building Dawn..."
args=""
args="${args} is_clang=true"
args="${args} is_component_build=true"
args="${args} is_debug=false"
args="${args} target_cpu=\"x64\""
gn gen "out/Shared" --args="${args}"
ninja -C "out/Shared"
cd "${WD}"

echo "Setting path to Dawn..."
printf -- "${SD}/submodules/dawn" >"${SD}/submodules/webgpu/PATH_TO_DAWN"

echo "Setting path to Node-API..."
printf -- "${SD}/submodules/node" >"${SD}/submodules/webgpu/PATH_TO_NODE_API"

echo "Setting path to Node-Addon_API..."
printf -- "${SD}/submodules/node_addon_api" >"${SD}/submodules/webgpu/PATH_TO_NODE_ADDON_API"

echo "Building WebGPU..."
cd "${SD}/submodules/webgpu"
npm ci
npm run all --dawnversion=0.0.1
echo "Building WebGPU Examples..."
cd examples
npm ci
rm -r ./node_modules/webgpu/generated/0.0.1/${OS}/build/Release
mkdir ./node_modules/webgpu/generated/0.0.1/${OS}/build/Release
cd ..
if [ "$OS" = "darwin" ]; then
  install_name_tool -add_rpath '@executable_path' ./generated/0.0.1/${OS}/build/Release/addon-${OS}.node
else
  chrpath -r "\$ORIGIN" ./generated/0.0.1/${OS}/build/Release/addon-${OS}.node
fi
cp ./generated/0.0.1/${OS}/build/Release/addon-${OS}.node ./examples/node_modules/webgpu/generated/0.0.1/${OS}/build/Release/addon-${OS}.node
echo "Examples now using this WebGPU N-API add-on instead of the npm version."
cd "${WD}"

echo "Initializing CTS..."
cp "${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/addon-${OS}.node" "${SD}/submodules/cts/third_party/dawn/index.node"
if [ "$OS" = "darwin" ]; then
  EXTENSION=.dylib
else
  EXTENSION=.so
fi
cp \
"${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/libc++${EXTENSION}" \
"${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/libdawn_native${EXTENSION}" \
"${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/libdawn_proc${EXTENSION}" \
"${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/libdawn_wire${EXTENSION}" \
"${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/libshaderc_spvc${EXTENSION}" \
"${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release/libshaderc${EXTENSION}" \
"${SD}/submodules/cts/third_party/dawn/"
cd "${SD}/submodules/cts"
npm ci
echo "Running CTS..."
npx grunt pre
cd "${WD}"

echo "Copying to install directory..."
mkdir -p ./install
cp "${SD}/submodules/cts/LICENSE.txt" ./install/LICENSE.cts.txt
cp "${SD}/submodules/dawn/LICENSE" ./install/LICENSE.dawn.txt
cp "${SD}/submodules/webgpu/LICENSE" ./install/LICENSE.webgpu.txt
cp -r "${SD}/submodules/webgpu/generated/0.0.1/${OS}/build/Release" ./install/
rm -rf ./install/Release/obj.target
rm -rf ./install/Release/.deps
